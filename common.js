window.addEventListener('load', function() {
    openTab(document.getElementsByClassName('tablinks')[0], 'tablinks', 'tabcontent', 'Visualization');
    openTab(document.getElementsByClassName('replinks')[0], 'replinks', 'repcontent', 'Matrix');
    openTab(document.getElementsByClassName('alglinks')[0], 'alglinks', 'algcontent', 'dfs');
    openTab(document.getElementsByClassName('dfsalglinks')[0], 'dfsalglinks', 'dfsalgcontent', 'Connectivity');
});

function openTab(currentTarget, buttonGroup, tabGroup, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName(tabGroup);
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName(buttonGroup);
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    currentTarget.className += " active";
}

function isGraphDirected() {
    var tmpDir = document.querySelector('input[name="direct"]:checked').value;
    return tmpDir == 'directed';
}

function checkGraphConnectivity() {
    console.debug("CheckGraphConectivity callback.");
    graphIsConnected(getGraph('list'), isGraphDirected());
}
