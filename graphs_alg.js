function graphUndirectify(graph) {
    var tmp = Object.assign({}, graph);
    for (var node in tmp) {
        for (var idx in tmp[node]) {
            var edge = tmp[node][idx];
            if (tmp[edge].indexOf(Number(node)) == -1) {
                tmp[edge].push(Number(node));
            }
        }
    }
    return tmp;
}

function graphInvertEdges(graph) {
    var tmp = {};
    for (var node in graph) {
        tmp[node] = [];
    }
    for (var node in graph) {
        for (var idx in graph[node]) {
            var edge = graph[node][idx];
            tmp[edge].push(Number(node));
        }
    }
    return tmp;
}

function graphIsConnectedDir(graph) {
    // use the same algorithm to check if graph is strongly connected
    var [stack, nodesAll] = graphNodeList(graph);
    var startNode = stack[Math.floor(Math.random()*stack.length)];
    stack = [startNode];
    var result = graphCheckConnectivity(graph, stack, nodesAll);
    if (result[0] == false) {
        // convert to undirected graph
        var tmp = graphUndirectify(graph);
        result = graphIsConnectedNonDir(tmp);
        if (result[0] == false) {
            return result;
        }
        return [true, "Graph is weakly connected."];
    } else {
        // check if graph is connected when edges are inverted 
        // to make sure that it is strongly connected
        for (node in nodesAll) {
            nodesAll[node] = false;
        }
        var tmp = graphInvertEdges(graph);
        stack = [startNode];
        result = graphCheckConnectivity(tmp, stack, nodesAll);
        if (result[0] == true) {
            return [true, "Graph is strongly connected."];
        } else {
            return [true, "Graph is weakly connected."];
        }
    }
}

function graphNodeList(graph) {
    var stack = [];
    var nodesAll = {};
    for(var nodeid in graph) {
        nodesAll[nodeid] = false;
        stack.push(Number(nodeid));
    }
    return [stack, nodesAll];
}

function graphCheckConnectivity(graph, stack, nodesAll) {
    while(stack.length > 0) {
        var current = stack.pop();
        nodesAll[current] = true;
        for(idx in graph[current]) {
            if (nodesAll[graph[current][idx]] == false) {
                stack.push(graph[current][idx]);
            }
        }
    }
    for (node in nodesAll) {
        if (nodesAll[node] == false) {
            return [false, "Graph is not connected"];
        }
    }
    return [true, "Graph is connected"];
}

function graphIsConnectedNonDir(graph) {
    var [stack, nodesAll] = graphNodeList(graph);
    stack = [stack[Math.floor(Math.random()*stack.length)]];
    return graphCheckConnectivity(graph, stack, nodesAll);
}

function graphIsConnected(graph, directed) {
    if (Object.keys(graph).length == 0) {
        var tmp = document.getElementById("graphConnectivity");
        tmp.innerHTML = "Graph is empty";
        return;
    }
    var result = [false, ''];
    if (directed) {
        result = graphIsConnectedDir(graph);
    } else {
        result = graphIsConnectedNonDir(graph);
    }
    var tmp = document.getElementById("graphConnectivity");
    tmp.innerHTML = result[1];
}
