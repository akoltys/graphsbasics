const node_radius = 30;

function getGraph(type = 'list') {
    if (type == 'list') {
        console.debug("Returning list: ", gNodeList);
        var result = {};
        for (var nodeid in gNodeList) {
            result[nodeid] = [];
            for (var idx in gNodeList[nodeid]) {
                result[nodeid].push(gNodeList[nodeid][idx]);
            }
        }
        return result;
    } else {
        console.debug("Returning matrix", gNodeMatrix);
        var result = [];
        for (var idx in gNodeMatrix) {
            var tmp = [];
            for (var jdx in gNodeMatrix[idx]) {
                tmp.push(gNodeMatrix[idx][jdx]);
            }
            result.push(tmp);
        }
        return ;
    }
}

function initGraphStructs(nodesNum) {
    for (var idx = 0; idx < nodesNum; idx++) {
        var tmpRow = [];
        for (var jdx = 0; jdx < nodesNum; jdx++) {
            tmpRow.push(0);
        }
        gNodeMatrix.push(tmpRow);
        gNodeList[idx] = []; 
    }
}

function generateDirGraph(nodesNum) {
    for (var idx = 0; idx < nodesNum; idx++) {
        for (var jdx = 0; jdx < nodesNum; jdx++) {
            if (idx != jdx) {
                var tmp = Math.random();
                if (tmp < 0.2 && gNodeMatrix[idx][jdx] != 1) {
                    gNodeMatrix[idx][jdx] = 1 ;
                    gNodeList[idx].push(jdx);
                }
            }
        }
     }
     console.debug("Gen List:   ", gNodeList);
     console.debug("Gen Matrix: ", gNodeMatrix);
}

function generateNonDirGraph(nodesNum) {
    for (var idx = 0; idx < nodesNum; idx++) {
        for (var jdx = idx+1; jdx < nodesNum; jdx++) {
            if (idx != jdx) {
                var tmp = Math.random();
                if (tmp < 0.2 && gNodeMatrix[idx][jdx] != 1) {
                    gNodeMatrix[idx][jdx] = 1;
                    gNodeMatrix[jdx][idx] = 1;
                    gNodeList[idx].push(jdx);
                    gNodeList[jdx].push(idx);
                }
            }
        }
    }
}

function generateGraph()
{
    var graphSize = document.getElementById("graphSize");
    var nodesNum = Number(graphSize.value);
    if (nodesNum == NaN || nodesNum <= 0 || nodesNum > 10) {
        return;
    }
    gNodeMatrix = [];
    gNodeList = {};
    initGraphStructs(nodesNum);
    if (isGraphDirected()) {
        generateDirGraph(nodesNum);
    } else {
        generateNonDirGraph(nodesNum);
    }
}

function drawEdgeDir(cvs, startNode, endNode) {
    var ctx = cvs.getContext("2d");
    ctx.beginPath();
    var fromX = gNodes[startNode].x;
    var fromY = gNodes[startNode].y;
    var toX = gNodes[endNode].x;
    var toY = gNodes[endNode].y;
    var dx = (toX-fromX);
    var dy = (toY-fromY)
    var angle = -Math.atan2(dy, dx);
    var startOffsetX = node_radius*Math.sin(angle);
    var endOffsetX = node_radius*Math.sin(angle);
    var startOffsetY = node_radius*Math.cos(angle);
    var endOffsetY = node_radius*Math.cos(angle);
    const headlen = 10;
    fromX = fromX + startOffsetX;
    fromY = fromY + startOffsetY;
    toX = toX + endOffsetX;
    toY = toY + endOffsetY;
    ctx.moveTo(fromX, fromY);
    ctx.lineTo(toX, toY);
    ctx.moveTo(toX-dx/2, toY-dy/2);
    ctx.lineTo(toX-dx/2 - headlen * Math.sin(angle - Math.PI / 6 + Math.PI/2), 
               toY-dy/2 - headlen * Math.cos(angle - Math.PI / 6 + Math.PI/2));
    ctx.moveTo(toX-dx/2, toY-dy/2);
    ctx.lineTo(toX-dx/2 - headlen * Math.sin(angle + Math.PI / 6 + Math.PI/2), 
               toY-dy/2 - headlen * Math.cos(angle + Math.PI / 6 + Math.PI/2));
    ctx.stroke();
}

function drawEdgeNonDir(cvs, startNode, endNode) {
    var ctx = cvs.getContext("2d");
    ctx.beginPath();
    var fromX = gNodes[startNode].x;
    var fromY = gNodes[startNode].y;
    var toX = gNodes[endNode].x;
    var toY = gNodes[endNode].y;
    var dx = (toX-fromX);
    var dy = (toY-fromY);
    var angle = -Math.atan2(dy, dx)+Math.PI/2;
    var startOffsetX = node_radius*Math.sin(angle);
    var endOffsetX = -node_radius*Math.sin(angle);
    var startOffsetY = node_radius*Math.cos(angle);
    var endOffsetY = -node_radius*Math.cos(angle);
    fromX = fromX + startOffsetX;
    fromY = fromY + startOffsetY;
    toX = toX + endOffsetX;
    toY = toY + endOffsetY;
    ctx.moveTo(fromX, fromY);
    ctx.lineTo(toX, toY);
    ctx.stroke();
}

function drawEdge(cvs, startNode, endNode) {
    if (isGraphDirected()) {
        drawEdgeDir(cvs, startNode, endNode);
    } else {
        drawEdgeNonDir(cvs, startNode, endNode);
    }
}

function drawNode(cvs, posx, posy, idx) {
    var ctx = cvs.getContext("2d");
    ctx.beginPath();
    ctx.arc(posx, posy, node_radius, 0, 2*Math.PI);
    ctx.fillStyle = "#F00DC0";
    ctx.fill();
    ctx.font = "30px Arial";
    ctx.fillStyle = "#000000";
    ctx.fillText(""+idx, posx, posy);
}

function clearGraph() {
    var cvs = document.getElementById("gCvs");
    var ctx = cvs.getContext("2d");
    ctx.clearRect(0, 0, cvs.width, cvs.height);
    gNodes = {};
}

function drawNodes(cvs, nodesNum) {
    const stepOffset = Math.random()*2*Math.PI
    const stepSize = 2*Math.PI/nodesNum;
    const radiusSize = 250;
    for (var idx = 0; idx < nodesNum; idx++) {
        var radius = radiusSize + Math.random()*(-100);
        var posX = Math.floor(cvs.width/2+radius*Math.sin(stepOffset + stepSize*idx));
        var posY = Math.floor(cvs.height/2+radius*Math.cos(stepOffset + stepSize*idx));
        drawNode(cvs, posX, posY, idx);
        gNodes[idx] = {x: posX, y: posY};
    }
}

function drawEdges(cvs, nodesNum) {
    for (var idx = 0; idx < nodesNum; idx++) {
        var offset = 0;
        if (isGraphDirected()) {
            offset = idx + 1;
        }
        for (var jdx = 0; jdx < nodesNum; jdx++) {
            if (gNodeMatrix[idx][jdx] != 0) {
                drawEdge(cvs, idx, jdx);
            }
        }
    }
}

function drawGraph() {
    var graphSize = document.getElementById("graphSize");
    var cvs = document.getElementById("gCvs");
    var nodesNum = Number(graphSize.value);
    if (nodesNum == NaN || nodesNum <= 0 || nodesNum > 10) {
        return;
    }
    clearGraph();
    drawNodes(cvs, nodesNum);
    drawEdges(cvs, nodesNum);
}

function clearDiv(divName) {
    var parent = document.getElementById(divName);
    while (parent.firstChild) {
        parent.removeChild(parent.lastChild);
    }
}

function updateMatrixValDir(pos) {
    var posX = Number(pos[0]);
    var posY = Number(pos[1]);
    console.debug("Updating matrix posx: ", posX, " posY: ", posY);
    if (posX == posY) {
        console.debug("Cannot modify diagonal values");
        return;
    }
    gNodeMatrix[posX][posY] = (gNodeMatrix[posX][posY]+1)%2;
}

function updateMatrixValNonDir(pos) {
    var posX = Number(pos[0]);
    var posY = Number(pos[1]);
    console.debug("Updating matrix posx: ", posX, " posY: ", posY);
    if (posX == posY) {
        console.debug("Cannot modify diagonal values");
        return;
    }
    gNodeMatrix[posX][posY] = (gNodeMatrix[posX][posY]+1)%2;
    gNodeMatrix[posY][posX] = (gNodeMatrix[posY][posX]+1)%2;
}

function updateMatrixVal(pos) {
    if (isGraphDirected()) {
        updateMatrixValDir(pos);
    } else {
        updateMatrixValNonDir(pos);
    }
}

function updateListValDir(pos) {
    var nodeid = Number(pos[0]);
    var ngbrid = Number(pos[1]);
    if (nodeid == ngbrid) {
        return;
    }
    console.debug("Updating list node: ", nodeid, " ngbr: ", ngbrid);
    console.debug("Node: ", nodeid, " ngbrlist: ", gNodeList[nodeid]);
    var idx = gNodeList[nodeid].indexOf(ngbrid);
    if (idx >= 0) {
        gNodeList[nodeid].splice(idx, 1);
    } else {
        gNodeList[nodeid].push(ngbrid);
        gNodeList[nodeid].sort();
    }
}

function updateListValNonDir(pos) {
    var nodeid = Number(pos[0]);
    var ngbrid = Number(pos[1]);
    if (nodeid == ngbrid) {
        return;
    }
    console.debug("Updating list node: ", nodeid, " ngbr: ", ngbrid);
    var idx = gNodeList[nodeid].indexOf(ngbrid);
    if (idx >= 0) {
        gNodeList[nodeid].splice(gNodeList[nodeid].indexOf(ngbrid), 1);
        gNodeList[ngbrid].splice(gNodeList[ngbrid].indexOf(nodeid), 1);
    } else {
        gNodeList[nodeid].push(ngbrid);
        gNodeList[nodeid].sort();
        gNodeList[ngbrid].push(nodeid);
        gNodeList[ngbrid].sort();
    }
}

function updateListVal(pos) {
    if (isGraphDirected()) {
        updateListValDir(pos);
    } else {
        updateListValNonDir(pos);
    }
}

var createClickHandler = function(argx, argy) {
    return function() { 
        console.debug("Update x: ", argx, " y: ", argy);
        updateMatrixVal([argx, argy]);
        updateListVal([argx, argy]);
        updateView();
     };
}

function createMatrixHeaderElement(val) {
    var tmpTd = document.createElement('td');
    tmpTd.className = 'representationheader';
    tmpTd.innerHTML = val;
    return tmpTd;
}

function drawMatrix() {
    clearDiv('MatrixData');
    var matrix = document.getElementById("MatrixData");
    var tmpTable = document.createElement('table');
    var tmpHeadRow = document.createElement('tr');
    tmpHeadRow.appendChild(createMatrixHeaderElement(''));
    for (var idx = 0; idx < gNodeMatrix.length; idx++) {
        tmpHeadRow.appendChild(createMatrixHeaderElement(idx));
    }
    tmpTable.appendChild(tmpHeadRow);
    for (var idx = 0; idx < gNodeMatrix.length; idx++) {
        var tmpTr = document.createElement('tr');
        tmpTr.appendChild(createMatrixHeaderElement(idx));
        for(var jdx = 0; jdx < gNodeMatrix.length; jdx++) {
            var tmpTd = document.createElement('td');
            tmpTd.innerHTML = gNodeMatrix[idx][jdx];
            tmpTd.onclick = createClickHandler(idx, jdx);
            tmpTr.appendChild(tmpTd);
        }
        tmpTable.appendChild(tmpTr);
    }
    matrix.appendChild(tmpTable);
}

function createListElement(value, header = false, callback = false, posx = 0, posy = 0) {
    var tmpTd = document.createElement('td');
    tmpTd.innerHTML = value;
    if (header) {
        tmpTd.className = 'representationheader';
    }
    if (callback) {
        tmpTd.onclick = createClickHandler(posx, posy);
    }
    return tmpTd;
}

function createAddElement(nodeid) {
    var tmpTd = document.createElement('td');
    var tmpBtn = document.createElement('button');
    var tmpInput = document.createElement('input');
    tmpInput.type = 'number';
    tmpInput.className = 'listAddInput';
    tmpBtn.innerHTML = "Add";
    tmpBtn.onclick = function () {
        var graphSize = document.getElementById("graphSize");
        var nodesNum = Number(graphSize.value);
        var ngbr = Number(tmpInput.value);
        if (ngbr == NaN || ngbr >= nodesNum || ngbr < 0 || nodeid == ngbr || gNodeList[nodeid].indexOf(ngbr) != -1) {
            return;
        }
        updateMatrixVal([nodeid, tmpInput.value]);
        updateListVal([nodeid, tmpInput.value]);
        updateView();
    }
    tmpTd.appendChild(tmpInput);
    tmpTd.appendChild(tmpBtn);
    return tmpTd;
}

function drawList() {
    clearDiv('ListData');
    var nodeList = document.getElementById("ListData");
    var tmpTable = document.createElement('table');
    for (var node in gNodeList) {
        console.debug("drawList Start: ", gNodeList[node]);
        var tmpTr = document.createElement('tr');
        tmpTr.appendChild(createListElement(node, true))
        for(var ngbr in gNodeList[node]) {
            tmpTr.appendChild(createListElement('&rArr;'));
            tmpTr.appendChild(createListElement(gNodeList[node][ngbr], false, true, node, gNodeList[node][ngbr]));
        }
        tmpTr.appendChild(createListElement('&rArr;'));
        tmpTr.appendChild(createListElement('NULL'));
        tmpTr.appendChild(createAddElement(node));
        tmpTable.appendChild(tmpTr);
        console.debug("drawList Stop: ", gNodeList[node]);
    }
    nodeList.appendChild(tmpTable);
}

function updateView() {
    drawGraph();
    drawMatrix();
    console.debug("UpdatView list1:  ", gNodeList);
    drawList();
    console.debug("UpdatView list2:  ", gNodeList);
    console.debug("UpdatView matrix: ", gNodeMatrix);
}

function generate() {
    generateGraph();
    updateView();
}
